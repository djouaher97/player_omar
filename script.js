let tracks = [
    {   url : './audio/Ghostrifter Official - Midnight Stroll.mp3' , 
        cover : './cover/cover-3.jpg', artist : 'Ghostrifter' , title : 'Midnight Stroll'},
    {   url : './audio/Ghostrifter Official - Hot Coffee.mp3' , 
        cover : './cover/cover-1.jpg', artist : 'Ghostrifter' , title : 'Hot Coffee'},
    {   url : './audio/Ghostrifter Official - Merry Bay.mp3' , 
        cover : './cover/cover-2.jpg', artist : 'Ghostrifter' , title : 'Merry Bay'},
    {   url : './audio/Ghostrifter Official - Morning Routine.mp3' , 
        cover : './cover/cover-4.jpg', artist : 'Ghostrifter' , title : 'Morning Routine'},
    {   url : './audio/Ghostrifter Official - Still Awake.mp3' , 
        cover : './cover/cover-5.jpeg', artist : 'Ghostrifter' , title : 'Still Awake'},
    {   url : './audio/Ghostrifter Official - Afternoon Nap.mp3' , 
        cover : './cover/cover-6.jpg', artist : 'Ghostrifter' , title : 'Afternoon Nap'},
    {   url : './audio/Ghostrifter Official - Demised To Shield.mp3' , 
        cover : './cover/cover-7.jpg', artist : 'Ghostrifter' , title : 'Demised To Shield'},
    {   url : './audio/Ghostrifter Official - Departure.mp3' , 
        cover : './cover/cover-8.jpg', artist : 'Ghostrifter' , title : 'Departure'},
    {   url : './audio/Ghostrifter Official - Mellow Out.mp3' , 
        cover : './cover/cover-9.jpg', artist : 'Ghostrifter' , title : 'Mellow Out'},
    {   url : './audio/Ghostrifter Official - On My Way.mp3' , 
        cover : './cover/cover-10.jpg', artist : 'Ghostrifter' , title : 'On My Way'},
    {   url : './audio/Ghostrifter Official - Simplicit Nights.wav' , 
        cover : './cover/cover-1.jpg', artist : 'Ghostrifter' , title : 'Simplicit Nights'},
    {   url : './audio/Ghostrifter Official - Soaring.mp3' , 
        cover : './cover/cover-2.jpg', artist : 'Ghostrifter' , title : 'Soaring'},
    {   url : './audio/Ghostrifter Official - Subtle Break.mp3' , 
        cover : './cover/cover-3.jpg', artist : 'Ghostrifter' , title : 'Subtle Break'},
    {   url : './audio/Ghostrifter Official - Transient.mp3' , 
        cover : './cover/cover-4.jpg', artist : 'Ghostrifter' , title : 'Transient'},
]




//traccia audio
const track = document.querySelector('#track');

//bottoni di azione
const playBtn = document.querySelector('#play-btn');
const pauseBtn = document.querySelector('#pause-btn');
const btnSidebar = document.querySelectorAll('.btn-sidebar')
const trackTitle = document.querySelector('#trackTitle')
const trackArtist = document.querySelector('#trackArtist')
const btnNextTrack = document.querySelector('#btnNextTrack')
const btnPrevTrack = document.querySelector('#btnPrevTrack')



//elementi
const cover = document.querySelector('.album-cover');
const playlistWrapper =  document.querySelector('#playlist-wrapper')

let currentTrack = 0;
let playing = false; //sta suonando il player?


//funzioni

function playPause(){
    playBtn.classList.toggle('d-none')
    pauseBtn.classList.toggle('d-none')
    cover.classList.toggle('play')
    if(playing){
        track.pause()
        playing = false
    } else {
        track.play()
        playing = true
    }
}


//track play
function play(){
    playBtn.classList.toggle('d-none');
    pauseBtn.classList.toggle('d-none');
    cover.classList.toggle('play');
    track.play()

}


//track pausa
function pause(){
    playBtn.classList.toggle('d-none');
    pauseBtn.classList.toggle('d-none');
    cover.classList.toggle('play');
    track.pause()
    
}

//popoliamo la lista di canzoni
function populateSongList(){
    tracks.forEach((track,index) => {

        let card = document.createElement('div')
        
        card.classList.add('col-12' , 'col-md-7')

        card.innerHTML=
        `
        <div class="song-cart">
            <p class="m-0 fw-bold" >${track.title}</p>
                <button class="btn btn-song-list" track-index = ${index}>
                    play <i class="far fa-play-circle"> </i>       
                </button>
        </div>
        
        `
        playlistWrapper.appendChild(card)

    })

}

function attachChangeSongEvent(){
    let btns = document.querySelectorAll('.btn-song-list')

    btns.forEach(btn => {
        btn.addEventListener('click', () =>{
        let index = btn.getAttribute('track-index')

        changeSong(index)
        })
    })

}

//cambia i dettagli della traccia
function changeTrackDetails(){
    trackArtist.innerHTML = tracks[currentTrack].artist;
    trackTitle.innerHTML = tracks[currentTrack].title;
    cover.src = tracks[currentTrack].cover;
    track.src = tracks[currentTrack].url;
}

//cambia traccia in avanti
function nextTrack(){
    currentTrack++;

    if(currentTrack > tracks.length - 1){
        currentTrack = 0
    }

    changeTrackDetails()

    if (playing) {
        pause();
        play();
    }

}

//cambia traccia indietro
function prevTrack(){
    currentTrack--;


    if(currentTrack < tracks.length - 1){
        currentTrack = tracks.length - 1
    }

    changeTrackDetails()

    if (playing) {
        pause();
        play();
    }


}

//Cambiare canzone dalla lista
function changeSong(index){
    currentTrack = index
    changeTrackDetails()

    pause()
    play()

}





//attacco eventi a bottoni
playBtn.addEventListener('click' , () =>{
    playPause()
})

pauseBtn.addEventListener('click' , () =>{
    playPause()
})

btnSidebar.forEach(btn => {
    btn.addEventListener('click' ,() =>{
        let selectedMenu = btn.getAttribute('btn-menu')
        
        let menu = document.querySelector(`.menu-wrapper-${selectedMenu}`)

        let sections = document.querySelectorAll('.menu-wrapper')
        
        sections.forEach((section) => {
            section.classList.add('d-none')
        })
        
        btnSidebar.forEach((btn) => {
            btn.classList.remove('active')
            
        }) 
        
        btnSidebar.forEach((btn) => {
            btn.classList.add('active')

        })

        menu.classList.add('d-block')
        menu.classList.remove('d-none')
        
        
    })
})

btnNextTrack.addEventListener('click',() =>{
    nextTrack();
})

btnPrevTrack.addEventListener('click',() =>{
    prevTrack();
})








//avvio delle funzioni
populateSongList();
changeTrackDetails();
attachChangeSongEvent();