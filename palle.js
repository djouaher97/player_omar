function random(max , min){
    return Math.floor(Math.random() * (max - min) + min)
}
class Vector {
    constructor(x,y){
        this.y = x;
        this.x = y;
    }
}
class Ball {
    constructor(window,r){
        this.y = random(window.innerHeight - r, r);
        this.x = random(window.innerWidth - r, r);
        this.width = r;
        this.height = r;
        this.limitX = window.innerWidth;
        this.limitY = window.innerHeight;
        this.vel = new Vector(random(-5 , 10) , random(-5 , 10))
        this.gradientA = `rgba(${random(0,255)} , ${random(0,255)} , ${random(0,255)}, 0.9)`;
        this.gradientB = `rgba(${random(0,255)} , ${random(0,255)} , ${random(0,255)}, 0.7)`;
    }

    create() {
        let b = document.createElement('span');
        b.style.display =  'inline-block';
        b.style.width =  this.width + 'px';
        b.style.height =  this.height + 'px';
        b.style.borderRadius =  '100%';
        b.style.position =  'absolute';
        b.style.transition =  'all 0.1s linear';
        b.style.top =  this.y + 'px';
        b.style.left =  this.x + 'px';
        b.style.zIndex = -1;
        // b.classList.add('hoverBig')
        document.body.appendChild(b)

        this.domObj = b;
    }

    update(){
        this.x += this.vel.x;
        this.y += this.vel.y;
        
        this.domObj.style.top = this.y + 'px';
        this.domObj.style.left = this.x + 'px' 
    }

    bounce() {
        if(this.x <= 0 || this.x >= this.limitX - this.width) this.vel.x *= -1
        if(this.y <= 0 || this.y >= this.limitY - this.height) this.vel.y *= -1
    }

    prospectiveLight(){
        let posX = this.domObj.offsetLeft + (this.width / 2)
        let posY = this.domObj.offsetTop + (this.height / 2)

        let focalX = 100 - (posX * 100 / this.limitX) 
        let focalY = 100 - (posY * 100 / this.limitY) 

        this.domObj.style.background =  `radial-gradient(circle at ${focalX}% ${focalY}% , ${this.gradientA} , ${this.gradientB}`;
    }

    play(){
        this.prospectiveLight()

        setInterval(() => {
            this.bounce()
            this.prospectiveLight()
            this.update()

        } , 60)
    }
}

for (let i = 1; i <= 10; i++) {
    let a = new Ball(window, 80 * (1 + Math.random()))
    a.create();
    a.play();
}
